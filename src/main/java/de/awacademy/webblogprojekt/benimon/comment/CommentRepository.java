package de.awacademy.webblogprojekt.benimon.comment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CommentRepository extends JpaRepository<CommentEntity, Long> {
    List<CommentEntity> findAllByOrderByCreationDateAsc();

    @Transactional
    void deleteAllByArticleEntityId(long id);

    CommentEntity findById(long id);

}
