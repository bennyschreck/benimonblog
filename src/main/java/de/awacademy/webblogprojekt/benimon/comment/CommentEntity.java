package de.awacademy.webblogprojekt.benimon.comment;

import de.awacademy.webblogprojekt.benimon.article.ArticleEntity;
import de.awacademy.webblogprojekt.benimon.user.UserEntity;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@Entity
public class CommentEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Type(type = "text")
    private String commentText;


    private Instant creationDate;

    @ManyToOne
    private UserEntity userEntity;

    @ManyToOne
    private ArticleEntity articleEntity;

    public CommentEntity() {
    }

    public CommentEntity(String commentText, ArticleEntity articleEntity, UserEntity userEntity) {

        this.commentText = commentText;
        this.creationDate = Instant.now();
        this.articleEntity = articleEntity;
        this.userEntity = userEntity;
    }

    public String getCommentText() {
        return commentText;
    }

    public ArticleEntity getArticleEntity() {
        return articleEntity;
    }

    public void setArticleEntity(ArticleEntity articleEntity) {
        this.articleEntity = articleEntity;
    }

    public String getCreationDate() {
        return LocalDateTime.ofInstant(creationDate, ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
    }

    public long getId() {
        return id;
    }

    public String getUser() {
        return userEntity.getUserName();
    }
}
