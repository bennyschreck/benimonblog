package de.awacademy.webblogprojekt.benimon.comment;


import javax.validation.constraints.NotEmpty;

public class CommentDTO {

    //    @NotEmpty
    private String commentText = "";

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    //    @NotEmpty
    private String headline = "";

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }


}
