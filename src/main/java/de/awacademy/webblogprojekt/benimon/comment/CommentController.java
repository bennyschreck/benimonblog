package de.awacademy.webblogprojekt.benimon.comment;

import de.awacademy.webblogprojekt.benimon.article.ArticleRepository;
import de.awacademy.webblogprojekt.benimon.user.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class CommentController {

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    CommentRepository commentRepository;

    @GetMapping("/comment")
    public String create(Model model) {
        model.addAttribute("comment", new CommentDTO());
        return "blog";
    }
//    @RequestMapping(value = "/comment", method = RequestMethod.POST)
//    public String create(Model model){
//        model.addAttribute("commet", new CommentDTO());
//        return "blog";
//    }

    @PostMapping("/comment")
    public String create(@RequestParam("articleId") long articleId, @ModelAttribute("comment") @Valid CommentDTO comment,
                         BindingResult bindingResult, @ModelAttribute("currentUser") UserEntity currentUser) {
        if (bindingResult.hasErrors()) {
            return "blog";
        }

        CommentEntity commentEntity = new CommentEntity(comment.getCommentText(), articleRepository.findById(articleId), currentUser);
        commentRepository.save(commentEntity);
        return "redirect:/";
    }

    @PostMapping("/deleteComment")
    public String delete(@RequestParam("commentId") long id, @ModelAttribute("currentUser") UserEntity currentUser) {
        if (currentUser != null || currentUser.isAdmin()) {
            commentRepository.deleteById(id);

            return "redirect:/";
        }

        if (currentUser == null || !currentUser.getCommentEntityList().contains(commentRepository.findById(id))) {
            return "redirect:/";
        }
        commentRepository.deleteById(id);

        return "redirect:/";
    }

}
