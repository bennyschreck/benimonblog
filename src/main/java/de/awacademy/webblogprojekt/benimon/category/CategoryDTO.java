package de.awacademy.webblogprojekt.benimon.category;

import javax.validation.constraints.NotEmpty;

public class CategoryDTO {

    @NotEmpty
    private String categoryName = "";

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }


}
