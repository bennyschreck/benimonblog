package de.awacademy.webblogprojekt.benimon.category;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {
    List<CategoryEntity> findAll();

    CategoryEntity findById(long id);

    CategoryEntity findByCategoryName(String categoryName);
}
