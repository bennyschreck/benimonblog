package de.awacademy.webblogprojekt.benimon.category;

import de.awacademy.webblogprojekt.benimon.article.ArticleEntity;

import javax.persistence.*;
import java.util.List;

@Entity
public class CategoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String categoryName;

    @ManyToMany(mappedBy = ("categoryEntityList"))
    private List<ArticleEntity> articleEntityList;

    public CategoryEntity(String categoryName) {
        this.categoryName = categoryName;
    }

    public CategoryEntity() {
    }

    public void setArticleEntityList(List<ArticleEntity> articleEntityList) {
        this.articleEntityList = articleEntityList;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public List<ArticleEntity> getArticleEntityList() {
        return articleEntityList;
    }

    public long getId() {
        return id;
    }
}
