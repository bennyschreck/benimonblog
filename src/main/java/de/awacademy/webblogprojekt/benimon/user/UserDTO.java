package de.awacademy.webblogprojekt.benimon.user;

import javax.validation.constraints.NotEmpty;

public class UserDTO {

    @NotEmpty
    private String role;

    private String userName;

    public String getUserName() {
        return userName;
    }
}
