package de.awacademy.webblogprojekt.benimon.user;

import de.awacademy.webblogprojekt.benimon.userRole.UserRoleEntity;
import de.awacademy.webblogprojekt.benimon.userRole.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserRoleRepository userRoleRepository;

    @GetMapping("/overview")
    public String overview_admin(Model model) {
        model.addAttribute("user", new UserDTO());
        model.addAttribute("users", userRepository.findAll());
        model.addAttribute("UserRoles", userRoleRepository.findAll());
        return "overview";
    }

    @PostMapping("/overview")
    public String changeRole(@RequestParam("roleId") String roleName, @RequestParam("person") long personId, @ModelAttribute("currentUser") UserEntity currentUser) {
        if (currentUser == null || !currentUser.isAdmin()) {
            return "redirect:/";
        }
        UserEntity changedUserEntity = userRepository.findById(personId);

        changedUserEntity.setRole(userRoleRepository.findByRoleName(roleName));

        userRepository.save(changedUserEntity);
        return "redirect:/overview";
    }


}
