package de.awacademy.webblogprojekt.benimon.archive;

import de.awacademy.webblogprojekt.benimon.article.ArticleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArchiveRepository extends JpaRepository<ArchiveEntity, Long> {
    List<ArchiveEntity> findByArticleEntityOrderByCreationDateDesc(ArticleEntity articleEntity);
}
