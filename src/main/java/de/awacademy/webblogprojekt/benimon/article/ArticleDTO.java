package de.awacademy.webblogprojekt.benimon.article;


import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import java.util.List;

public class ArticleDTO {
    //    @NotEmpty
    private String text = "";

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    private List<String> categoryEntityList;

    public List<String> getCategoryEntityList() {
        return categoryEntityList;
    }

    public void setCategoryEntityList(List<String> categoryEntityList) {
        this.categoryEntityList = categoryEntityList;
    }

    private MultipartFile file;

    //    @NotEmpty
    private String headline = "";

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public MultipartFile getFile() {
        return file;
    }

}
