package de.awacademy.webblogprojekt.benimon.article;

import de.awacademy.webblogprojekt.benimon.archive.ArchiveEntity;
import de.awacademy.webblogprojekt.benimon.category.CategoryEntity;
import de.awacademy.webblogprojekt.benimon.comment.CommentEntity;
import de.awacademy.webblogprojekt.benimon.user.UserEntity;
import org.hibernate.annotations.Type;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Entity
public class ArticleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String headline;

    @Type(type = "text")
    private String text;

    private Instant creationDate;

    @ManyToOne
    private UserEntity userEntity;

    @ManyToOne
    private UserEntity lastChangeUserEntity;

    @OneToMany(mappedBy = "articleEntity")
    private List<CommentEntity> commentEntityList;

    @ManyToMany
    private List<CategoryEntity> categoryEntityList;

    @OneToMany(mappedBy = "articleEntity")
    private List<ArchiveEntity> archiveEntityList;

    public ArticleEntity() {
    }

    public ArticleEntity(String headline, String text, UserEntity userEntity, List<CategoryEntity> categoryEntityList) {
        this.headline = headline;
        this.text = text;
        this.userEntity = userEntity;
        this.creationDate = Instant.now();
        this.categoryEntityList = categoryEntityList;
    }

    public List<ArchiveEntity> getArchiveEntityList() {
        return archiveEntityList;
    }

    public void setLastChangeUserEntity(UserEntity lastChangeUserEntity) {
        this.lastChangeUserEntity = lastChangeUserEntity;
    }

    public UserEntity getLastChangeUserEntity() {
        return lastChangeUserEntity;
    }

    public void addComment(CommentEntity commentEntity) {
        commentEntityList.add(commentEntity);
    }

    public String getHeadline() {
        return headline;
    }

    public String getText() {
        return text;
    }

    public String getCreationDate() {

        return LocalDateTime.ofInstant(creationDate, ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
    }

    public Instant getCreationDateTime() {
        return creationDate;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public String getUser() {
        return userEntity.getUserName();
    }

    public long getId() {
        return id;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void updateCreationDate() {
        this.creationDate = Instant.now();
    }

    public List<CommentEntity> getCommentEntityList() {
        return commentEntityList;
    }

    public List<CategoryEntity> getCategoryEntityList() {
        return categoryEntityList;
    }

    public void setCategoryEntityList(List<CategoryEntity> categoryEntityList) {
        this.categoryEntityList = categoryEntityList;
    }
}
