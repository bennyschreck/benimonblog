package de.awacademy.webblogprojekt.benimon.article;

import de.awacademy.webblogprojekt.benimon.category.CategoryEntity;
import de.awacademy.webblogprojekt.benimon.user.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ArticleRepository extends JpaRepository<ArticleEntity, Long> {
    List<ArticleEntity> findAllByOrderByCreationDateDesc();

    ArticleEntity findById(long id);

    List<ArticleEntity> findByUserEntity(UserEntity userEntity);

    void deleteById(long id);

    List<ArticleEntity> findByCategoryEntityList(List<CategoryEntity> categoryEntityList);

    List<ArticleEntity> findAllByCategoryEntityListOrderByCreationDateDesc(List<CategoryEntity> categoryEntityList);

    List<ArticleEntity> findAllByUserEntityIdOrderByCreationDateDesc(long id);

    List<ArticleEntity> findAllByUserEntityUserNameOrderByCreationDateDesc(String name);

}
