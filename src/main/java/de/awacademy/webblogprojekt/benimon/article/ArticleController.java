package de.awacademy.webblogprojekt.benimon.article;

import de.awacademy.webblogprojekt.benimon.archive.ArchiveEntity;
import de.awacademy.webblogprojekt.benimon.archive.ArchiveRepository;
import de.awacademy.webblogprojekt.benimon.category.CategoryDTO;
import de.awacademy.webblogprojekt.benimon.category.CategoryEntity;
import de.awacademy.webblogprojekt.benimon.category.CategoryRepository;
import de.awacademy.webblogprojekt.benimon.comment.CommentRepository;
import de.awacademy.webblogprojekt.benimon.user.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ArticleController {

    @Autowired
    ArticleRepository articleRepository;
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    ArchiveRepository archiveRepository;

    @GetMapping("/article")
    public String create(Model model) {
        model.addAttribute("article", new ArticleDTO());
        model.addAttribute("categories", categoryRepository.findAll());
        model.addAttribute("category", new CategoryDTO());
        return "create_article";
    }

    @PostMapping("/article")
    public String create(@ModelAttribute("article") @Valid ArticleDTO articleDTO, BindingResult bindingResult, @ModelAttribute("currentUser") UserEntity currentUser) {
        if (currentUser == null || !currentUser.isAdmin()) {
            return "redirect:/";
        }
        List<CategoryEntity> categoryEntityList = new ArrayList<>();
        if (articleDTO.getCategoryEntityList() != null) {
            for (String categoryname : articleDTO.getCategoryEntityList()) {
                categoryEntityList.add(categoryRepository.findByCategoryName(categoryname));
            }
        }
        ArticleEntity articleEntity = new ArticleEntity(articleDTO.getHeadline(), articleDTO.getText(), currentUser, categoryEntityList);
        articleRepository.save(articleEntity);
        return "redirect:/";
    }

    @GetMapping("/editArticle")
    public String edit(Model model, @RequestParam("articleId") long id) {

        model.addAttribute("articles", articleRepository.findById(id));
        ArticleDTO articleDTO = new ArticleDTO();
        articleDTO.setHeadline(articleRepository.findById(id).getHeadline());
        articleDTO.setText(articleRepository.findById(id).getText());
        model.addAttribute("article", articleDTO);

        CategoryDTO categoryDTO = new CategoryDTO();

        model.addAttribute("categories", categoryRepository.findAll());
        model.addAttribute("category", categoryDTO);


        return "edit_article";
    }

    @PostMapping("/editArticle")
    public String edit(@ModelAttribute("editArticle") @Valid ArticleDTO articleDTO, @RequestParam("articleId") long id, BindingResult bindingResult, @ModelAttribute("currentUser") UserEntity currentUser) {
        if (currentUser == null || !currentUser.isAdmin()) {
            return "redirect:/";
        }

        List<CategoryEntity> categoryEntityList = new ArrayList<>();
        if (articleDTO.getCategoryEntityList() != null) {
            for (String categoryname : articleDTO.getCategoryEntityList()) {
                categoryEntityList.add(categoryRepository.findByCategoryName(categoryname));
            }
        }
        ArticleEntity updateArticleEntity = articleRepository.findById(id);
        UserEntity userEntityForArchive;
        if (updateArticleEntity.getLastChangeUserEntity() == null) {
            userEntityForArchive = updateArticleEntity.getUserEntity();
        } else {
            userEntityForArchive = updateArticleEntity.getLastChangeUserEntity();
        }


        ArchiveEntity archiveEntity = new ArchiveEntity(updateArticleEntity.getText(), updateArticleEntity.getCreationDateTime(), updateArticleEntity.getHeadline(), updateArticleEntity, userEntityForArchive);

        updateArticleEntity.setHeadline(articleDTO.getHeadline());
        updateArticleEntity.setText(articleDTO.getText());
        updateArticleEntity.updateCreationDate();
        updateArticleEntity.setCategoryEntityList(categoryEntityList);
        updateArticleEntity.setLastChangeUserEntity(currentUser);
        articleRepository.save(updateArticleEntity);
        archiveRepository.save(archiveEntity);

        return "redirect:/";
    }


    @PostMapping("/deleteArticle")
    public String delete(@RequestParam("articleId") long id, @ModelAttribute("currentUser") UserEntity currentUser) {
        if (currentUser == null || !currentUser.isAdmin()) {
            return "redirect:/";
        }
        commentRepository.deleteAllByArticleEntityId(id);
        articleRepository.deleteById(id);

        return "redirect:/";
    }

}
