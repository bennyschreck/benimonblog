package de.awacademy.webblogprojekt.benimon.login;

import de.awacademy.webblogprojekt.benimon.session.SessionEntity;
import de.awacademy.webblogprojekt.benimon.session.SessionRepository;
import de.awacademy.webblogprojekt.benimon.user.UserEntity;
import de.awacademy.webblogprojekt.benimon.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Controller
public class LoginController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    SessionRepository sessionRepository;

    @GetMapping("login")
    public String login(Model model) {
        model.addAttribute("login", new LoginDTO());
        return "login";
    }

    @PostMapping("login")
    public String loginSubmit(Model model, LoginDTO login, HttpServletResponse response, @ModelAttribute("currentUser") UserEntity currentUser) {
        if (currentUser != null) {
            return "redirect:/";
        }
        Optional<UserEntity> optionalUser = userRepository.findFirstByUserNameAndPassword(login.getUsername(), login.getPassword());
        if (optionalUser.isPresent()) {
            SessionEntity s = new SessionEntity(optionalUser.get());
            sessionRepository.save(s);
            response.addCookie(new Cookie("sessionId", s.getId()));
            return "redirect:/";
        }
        model.addAttribute("login", login);
        return "login";
    }

    @PostMapping("logout")
    public String logout(@CookieValue(value = "sessionId", defaultValue = "") String sessionId, HttpServletResponse response, @ModelAttribute("currentUser") UserEntity currentUser) {
        if (currentUser == null) {
            return "redirect:/";
        }
        sessionRepository.deleteById(sessionId);
        Cookie c = new Cookie("sessionId", "");
        c.setMaxAge(0);
        response.addCookie(c);
        return "redirect:/";
    }
}
