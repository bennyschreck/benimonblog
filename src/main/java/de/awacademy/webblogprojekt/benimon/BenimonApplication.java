package de.awacademy.webblogprojekt.benimon;

import de.awacademy.webblogprojekt.benimon.user.UserEntity;
import de.awacademy.webblogprojekt.benimon.user.UserRepository;
import de.awacademy.webblogprojekt.benimon.userRole.UserRoleEntity;
import de.awacademy.webblogprojekt.benimon.userRole.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.io.IOException;

@SpringBootApplication
public class BenimonApplication {

    public static void main(String[] args) {
        SpringApplication.run(BenimonApplication.class, args);
    }


    @Autowired
    UserRoleRepository userRoleRepository;
    @Autowired
    UserRepository userRepository;

    //Methode um die Datenbank mit ein paar Werten zu füllen die dringend notwendig sind, dass das Programm funktioniert
    @PostConstruct
    public void dataFiller() throws IOException {
        if (userRoleRepository.findAll().size() == 0) {
            UserRoleEntity admin = new UserRoleEntity("Admin");
            UserRoleEntity standard = new UserRoleEntity("Standard");
            userRoleRepository.save(admin);
            userRoleRepository.save(standard);
        }
        if (userRepository.findByRole(userRoleRepository.findByRoleName("Admin")).size() == 0) {
            UserEntity admin = new UserEntity("Admin", "1234", userRoleRepository.findByRoleName("Admin"));
            userRepository.save(admin);
        }
    }
}
