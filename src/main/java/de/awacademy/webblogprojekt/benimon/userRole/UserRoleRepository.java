package de.awacademy.webblogprojekt.benimon.userRole;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRoleRepository extends JpaRepository<UserRoleEntity, Long> {
    List<UserRoleEntity> findAll();

    UserRoleEntity findByRoleName(String roleName);

    UserRoleEntity findById(long id);
}
