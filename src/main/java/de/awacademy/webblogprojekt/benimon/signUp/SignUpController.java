package de.awacademy.webblogprojekt.benimon.signUp;

import de.awacademy.webblogprojekt.benimon.user.UserEntity;
import de.awacademy.webblogprojekt.benimon.user.UserRepository;
import de.awacademy.webblogprojekt.benimon.userRole.UserRoleEntity;
import de.awacademy.webblogprojekt.benimon.userRole.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class SignUpController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserRoleRepository userRoleRepository;

    @GetMapping("signup")
    public String signup(Model model) {
        model.addAttribute("signup", new SignUpDTO());
        return "signup";
    }

    @PostMapping("signup")
    public String signup(@ModelAttribute("signup") @Valid SignUpDTO signup, BindingResult bindingResult, @ModelAttribute("currentUser") UserEntity currentUser) {
        if (currentUser != null) {
            return "redirect:/";
        }
        if (!signup.getPassword1().equals(signup.getPassword2())) {
            bindingResult.addError(new FieldError("signup", "password2", "Passwords do not match"));
        }

        if (userRepository.existsByUserName(signup.getUsername())) {
            bindingResult.addError(new FieldError("signup", "username", "Username taken"));
        }

        if (bindingResult.hasErrors()) {
            return "signup";
        }

        UserEntity user = new UserEntity(signup.getUsername(), signup.getPassword1(), userRoleRepository.findByRoleName("Standard"));
        userRepository.save(user);
        return "redirect:/login";
    }
}

