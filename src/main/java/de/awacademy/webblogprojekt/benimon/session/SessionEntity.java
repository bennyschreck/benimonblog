package de.awacademy.webblogprojekt.benimon.session;

import de.awacademy.webblogprojekt.benimon.user.UserEntity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.UUID;

@Entity
public class SessionEntity {
    @Id
    private String id;

    @ManyToOne
    private UserEntity user;

    public SessionEntity() {
    }

    public SessionEntity(UserEntity user) {
        this.user = user;
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public UserEntity getUser() {
        return user;
    }
}
