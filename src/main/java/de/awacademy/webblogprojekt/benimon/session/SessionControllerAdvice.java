package de.awacademy.webblogprojekt.benimon.session;

import de.awacademy.webblogprojekt.benimon.user.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Optional;

@ControllerAdvice
public class SessionControllerAdvice {

    @Autowired
    SessionRepository sessionRepository;

    @ModelAttribute("currentUser")
    public UserEntity currentUser(@CookieValue(value = "sessionId", defaultValue = "") String sessionId) {
        if (sessionId.length() > 0) {
            Optional<SessionEntity> sess = sessionRepository.findById(sessionId);
            if (sess.isPresent()) {
                return sess.get().getUser();
            }
        }
        return null;
    }
}

